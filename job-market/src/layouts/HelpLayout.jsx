import { NavLink, Outlet } from "react-router-dom"


const HelpLayout = () => {
    return (
        <div className="help-layout">

            <h2> Website Help</h2>
            <p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam explicabo nesciunt fugiat libero, quasi necessitatibus quae modi autem et praesentium quos commodi nisi ipsa at obcaecati eos. Possimus, nihil consequatur!</p>
           
           <nav>
            <NavLink to="faq"> View the FAQ</NavLink>
            <NavLink to="contact"> Contact Us </NavLink>
           </nav>
           
            <Outlet/>

        </div>
    )
}

export default HelpLayout