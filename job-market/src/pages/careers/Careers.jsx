import { useLoaderData, Link } from "react-router-dom";
import { BASE_SEARCH_URL, API_KEY, APP_ID } from "../../common/constants";

const Careers = () => {
    const careers = useLoaderData();

    return (
        <div className="careers">
            {careers.map((career) => (
                <Link to="/" key={career.id}>
                    <p> {career.company.display_name} </p>
                    <p>Based in {career.location.area.filter(val => val).join(", ")} </p>
                </Link>
            ))}
        </div>
    );

};

export const careersLoader = () => {
    return fetch(`${BASE_SEARCH_URL}?app_id=${APP_ID}&app_key=${API_KEY}&results_per_page=10&sort_by=date`)
        .then((response) => response.json())
        .then((data) => {
            return data.results;
        });
};

export default Careers;
