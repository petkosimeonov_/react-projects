import { useEffect, useState } from "react";
import { WeatherInfo, Forecast, Astronomy, Sports, SearchBox } from './index';
import { getCurrentWeather, getForecast, getAstronomy, getSports } from "../../common/api";
import {Box} from '@chakra-ui/react';

const Weather = () => {
    const [weather, setWeather] = useState(null);
    const [city, setCity] = useState("Sofia");
    const [input, setInput] = useState("");
    const [forecast, setForecast] = useState(null);
    const [astronomy, setAstronomy] = useState(null);
    const [sports, setSports] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        getCurrentWeather(city)
            .then((data) => {
                setWeather(data);
                setError(null);
            })
            .catch((error) => {
                console.error(error);
                setError("Error fetching weather data");
            })
    }, [city]);

    const changeCity = (e) => {
        e.preventDefault();
        setCity(input);
        setInput('');
    };

    useEffect(() => {
        getForecast(city)
            .then((data) => {
                setForecast(data);
                setError(null);
            })
            .catch((error) => {
                console.error(error);
                setError('Error fetching the forecast data');
            });
    }, [city]);

    useEffect(() => {
        getAstronomy(city)
            .then((data) => {
                setAstronomy(data);
                setError(null);
            })
            .catch((error) => {
                console.error(error);
                setError("Error fetching the astronomy data");
            });
    }, [city]);

    useEffect(() => {
        getSports(city)
            .then((data) => {
                setSports(data);
                setError(null);
            })
            .catch((error) => {
                console.error(error);
                setError("Error fetching the sports data");
            });
    }, [city]);

    return (
        <>
            <Box mb="2rem">
            <SearchBox
                value={input}
                onChange={(e) => setInput(e.target.value)}
                onSubmit={changeCity}
                error={error}
            />
            </Box>
            {weather && <WeatherInfo weather={weather} city={city} />}
            {error && <p>{error}</p>}
            {forecast && <Forecast forecastData={forecast} city={city} />}
            {error && <p>{error}</p>}
            {astronomy && <Astronomy astronomy={astronomy} city={city} />}
            {error && <p>{error}</p>}
            {sports && <Sports sports={sports} />}
            {error && <p>{error}</p>}
        </>
    );

};

export default Weather;
