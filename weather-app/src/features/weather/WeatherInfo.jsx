import PropTypes from 'prop-types';
import { Box, Text, Image } from "@chakra-ui/react";

const WeatherInfo = ({ weather, city }) => {

    const lastUpdated = new Date(weather.last_updated);
    const day = lastUpdated.getDate();
    const month = lastUpdated.toLocaleString('default', { month: 'long' })
    const year = lastUpdated.toLocaleString('default', { year: "numeric" });
    const hour = lastUpdated.toLocaleString([], { hour: "2-digit", minute: "2-digit" });
    const temperature = weather.temp_c;
    const conditions = weather.condition.text;

    const styles = {
        backgroundColor: "#2F3F58",
        borderRadius: '10px',
        padding: '20px',
        marginBottom: '20px'
    };

    return (
        <Box style={styles} textAlign="center" color="white">
            <Text fontSize="30px" mb="10px">{city}</Text>
            <Image
                src={weather.condition.icon}
                alt={weather.condition.text}
                boxSize="100px"
                mb="20px"
                mx="auto"
            />
            <Box>
                <Text fontSize="50px" fontWeight="bold" mb="10px">{temperature}°C</Text>
                <br />
                <Text fontSize="18px" lineHeight="1.5">
                    Last updated on {year}/{month}/{day}, {hour} local time <br />
                    The conditions are {conditions}
                </Text>
            </Box>
        </Box>
    );
};


WeatherInfo.propTypes = {
    weather: PropTypes.shape({
        last_updated: PropTypes.string.isRequired,
        temp_c: PropTypes.number.isRequired,
        condition: PropTypes.shape({
            text: PropTypes.string.isRequired,
            icon: PropTypes.string.isRequired,
        }).isRequired,
    }).isRequired,
    city: PropTypes.string.isRequired,
};


export default WeatherInfo