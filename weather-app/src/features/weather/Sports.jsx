import PropTypes from 'prop-types';
import { Box, Button, Fade, useDisclosure } from "@chakra-ui/react";

const Sports = ({ sports: { football } }) => {
    const { isOpen, onToggle } = useDisclosure();

    const matches = football.map((match, index) => {
        const countryOfMatch = match.country;
        const tournamentOfMatch = match.tournament;
        const matchToBePlayed = match.match;

        const styles = {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            fontFamily: 'Arial, sans-serif',
            fontWeight: 'bold',
            fontSize: '20px',
            lineHeight: '1.5',
            marginBottom: '20px'
        };

        const countryStyles = {
            fontSize: '30px',
            marginBottom: '10px',
            textDecoration: 'underline'
        };

        const tournamentStyles = {
            fontSize: '25px',
            marginBottom: '5px'
        };

        const matchStyles = {
            fontSize: '20px'
        };

        const buttonStyles = {
            size: 'lg',
            colorScheme: 'red',
            margin: '0 10px',
            transition: '0.3s',
            _hover: {
                transform: 'scale(1.1)',
                backgroundColor: 'green !important',
                color: 'white',
            },
            _active: {
                backgroundColor: 'green.400',
            }
        };

        return (
            <div style={styles} key={index}>
                <p style={countryStyles}>{countryOfMatch}</p>
                <p style={tournamentStyles}>Tournament: {tournamentOfMatch} 🏆</p>
                <p style={matchStyles}>Match: {matchToBePlayed} ⚽️</p>
                <p style={{ marginBottom: '5px', fontSize: '16px' }}>Who do you think is going to win? 🧐</p>
                <Box>
                    <Button style={buttonStyles} onClick={onToggle}>1</Button>
                    <Button style={buttonStyles} onClick={onToggle}>X</Button>
                    <Button style={buttonStyles} onClick={onToggle}>2</Button>
                </Box>
                <Fade in={isOpen}>
                    <Box
                        p='40px'
                        color='white'
                        mt='4'
                        bg='teal.500'
                        rounded='md'
                        shadow='md'
                        style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}
                    >
                        Are you confident in your choice? 😜
                    </Box>
                </Fade>
            </div>
        );
    });

    return (
        <div className="sports-football-info">
            <h2 style={{ textAlign: 'center', fontFamily: 'Arial, sans-serif', fontWeight: 'bold', fontSize: '30px' }}>Main football events for today 📅:</h2>
            <Box style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                {matches}
            </Box>
        </div>
    );
};

Sports.propTypes = {
    sports: PropTypes.shape({
        football: PropTypes.arrayOf(
            PropTypes.shape({
                country: PropTypes.string.isRequired,
                tournament: PropTypes.string.isRequired,
                match: PropTypes.string.isRequired,
            })
        ).isRequired,
    }).isRequired,
};

export default Sports;




