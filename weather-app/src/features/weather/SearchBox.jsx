import { Input } from "@chakra-ui/react";
import PropTypes from 'prop-types';
const SearchBox = ({ value, onChange, onSubmit, error, isDarkMode }) => {
    const borderColor = error ? "red.500" : "gray.300";
    const focusBorderColor = error ? "red.500" : "teal.500";
    const inputColor = isDarkMode ? "white" : "red.500";

    return (
        <form onSubmit={onSubmit}>
            <Input
                variant="filled"
                placeholder="Search for a location here"
                value={value}
                onChange={onChange}
                bg={isDarkMode ? "darkBlue" : "black"}
                color={inputColor}
                borderColor={borderColor}
                focusBorderColor={focusBorderColor}
                _hover={{
                    borderColor: focusBorderColor,
                }}
                _focus={{
                    boxShadow: "none",
                }}
                _placeholder={{
                    color: "gray.500",
                }}
            />
        </form>
    );
};


SearchBox.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    error: PropTypes.string,
    isDarkMode: PropTypes.bool,
};

export default SearchBox;


