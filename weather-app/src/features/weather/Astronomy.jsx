import PropTypes from 'prop-types';
import { Box } from '@chakra-ui/react';

const Astronomy = ({ astronomy: { astro }, city }) => {

    const sunrise = astro.sunrise;
    const sunset = astro.sunset;

    const styles = {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        backgroundColor: '#2F3F58',
        borderRadius: '10px',
        padding: '20px',
        marginBottom: '20px'
    }

    const timeStyles = {
        cursor: ' 0 0, pointer',
        textDecoration: 'underline',
        _hover: {
            textDecoration: 'none',
        },
    };



    return (
        <Box style={styles}>
            <div>
                Today in {city}, the sun rises at <span style={timeStyles} title="local time">{sunrise} 🌞</span> and sets at <span style={timeStyles} title="local time">{sunset} 🌇</span>.
            </div>
        </Box>
    )
}


Astronomy.propTypes = {
    astronomy: PropTypes.shape({
        astro: PropTypes.shape({
            sunrise: PropTypes.string.isRequired,
            sunset: PropTypes.string.isRequired,
        }).isRequired
    }).isRequired,
    city: PropTypes.string.isRequired,
}


export default Astronomy
