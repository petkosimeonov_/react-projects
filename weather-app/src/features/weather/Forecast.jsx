import PropTypes from 'prop-types';
import { Box, Center, Heading, Text, HStack } from "@chakra-ui/react";

const Forecast = ({ forecastData, city }) => {

    const weatherForecast = (day) => {
        const date = day.date;
        const maxTemp = day.day.maxtemp_c;
        const minTemp = day.day.mintemp_c;

        const dayString = new Date(date).toLocaleDateString('en-US', { weekday: 'long' });
        const monthString = new Date(date).toLocaleDateString('en-US', { month: 'short' });
        const dayOfMonthString = new Date(date).toLocaleDateString('en-US', { day: 'numeric' });

        return (
            <Box key={date} mb="1rem" w="100%">
                <Center>
                    <Heading fontSize="2xl" mb="0.5rem" fontFamily="Roboto">{dayString}, {monthString} {dayOfMonthString}</Heading>
                </Center>
                <HStack justifyContent="center" mb="0.5rem">
                    <Text fontSize="lg">Maximum temperature: {maxTemp} °C  😎 </Text>
                    
                </HStack>
                <HStack justifyContent="center">
                    <Text fontSize="lg">Minimum temperature: {minTemp} °C  😕 </Text>
                    
                </HStack>
            </Box>
        );
    };

    return (
        <Box textAlign="center" mt="2rem">
            <Heading fontSize="3xl" mb="1rem" fontFamily="Roboto">Weather Forecast for {city} for the next 3 days:</Heading>
            {forecastData && forecastData.map(day => weatherForecast(day))}
        </Box>
    );
};

Forecast.propTypes = {
    forecastData: PropTypes.arrayOf(PropTypes.shape({
        date: PropTypes.string.isRequired,
        day: PropTypes.shape({
            maxtemp_c: PropTypes.number.isRequired,
            mintemp_c: PropTypes.number.isRequired,
        }).isRequired,
    })).isRequired,
    city: PropTypes.string.isRequired,
};

export default Forecast;

