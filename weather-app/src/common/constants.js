export const API_KEY = '630ee4e57cc0473aac0214214232504';

export const BASE_URL = 'http://api.weatherapi.com/v1/current.json';

export const FORECAST_URL = 'http://api.weatherapi.com/v1/forecast.json';

export const ASTRONOMY_URL = 'http://api.weatherapi.com/v1/astronomy.json';

export const SPORTS_URL = 'http://api.weatherapi.com/v1/sports.json';

export const TODAY = new Date().toISOString().slice(0,10);

export const LOGO = '//cdn.weatherapi.com/v4/images/weatherapi_logo.png';