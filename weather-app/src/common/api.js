import {
  API_KEY,
  BASE_URL,
  FORECAST_URL,
  ASTRONOMY_URL,
  SPORTS_URL,
  TODAY,
} from "./constants";

export const getCurrentWeather = (city) => {
  return fetch(`${BASE_URL}?key=${API_KEY}&q=${city}`)
    .then((response) => response.json())
    .then((data) => data.current);
};

export const getForecast = (city) => {
  return fetch(`${FORECAST_URL}?key=${API_KEY}&q=${city}&days=3`)
    .then((response) => response.json())
    .then((data) => data.forecast.forecastday);
};

export const getAstronomy = (city) => {
  return fetch(`${ASTRONOMY_URL}?key=${API_KEY}&q=${city}&dt=${TODAY}`)
    .then((response) => response.json())
    .then((data) => data.astronomy);
};

export const getSports = (city) => {
  return fetch(`${SPORTS_URL}?key=${API_KEY}&q=${city}`).then((response) =>
    response.json()
  );
};
