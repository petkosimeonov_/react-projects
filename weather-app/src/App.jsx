import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";
function App() {

  return (
    <>
    <Header/>
    <br/>
    <Body/>
    <br/>
    <Footer/>
    </>
  )
}

export default App
