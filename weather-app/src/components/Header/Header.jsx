import { Button, Flex, useColorMode } from "@chakra-ui/react";
import { SunIcon, MoonIcon } from "@chakra-ui/icons";

const Header = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const isDark = colorMode === "dark";
  const buttonColor = isDark ? "white" : "blue.500";
  const buttonIcon = isDark ? <SunIcon color="yellow.300" /> : <MoonIcon />;

  return (
    <Flex
      as="header"
      align="center"
      justify="center"
      p="1rem"
      bgGradient="linear(to-r, #F56565, #ECC94B, #48BB78)"
      color="white"
      textShadow="2px 2px 2px rgba(0, 0, 0, 0.5)"
      fontSize="3xl"
      fontWeight="bold"
      cursor="pointer"
      transition="all 0.2s ease-in-out"
    >
      <h1 style={{ margin: 0 }}>Welcome to the Weather Application</h1>
      <Button
        ml="1rem"
        size="sm"
        onClick={toggleColorMode}
        rightIcon={buttonIcon}
        color={buttonColor}
      >
        {isDark ? "Light" : "Dark"}
      </Button>
    </Flex>
  );
};

export default Header;





