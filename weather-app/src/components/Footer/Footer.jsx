import { Box, Image, Link } from "@chakra-ui/react";
import { LOGO } from "../../common/constants";

const Footer = () => {
    return (
        <Box
            as="footer"
            p="1rem"
            bgGradient="linear(to-r, #F56565, #ECC94B, #48BB78)"
            color="white"
            textAlign="center"
            fontSize="sm"
            fontWeight="bold"
            textShadow="2px 2px 2px rgba(0, 0, 0, 0.5)"
        >
            <Link
                href="https://www.weatherapi.com/"
                title="Free Weather API"
                _hover={{
                    textDecoration: "none",
                    transform: "scale(1.05)",
                    transition: "all 0.2s ease-in-out",
                }}
                display="inline-block"
                mx="auto"
            >
                <Image
                    src={LOGO}
                    alt="Weather data by WeatherAPI.com"
                    maxH="40px"
                />
            </Link>
        </Box>
    );
};

export default Footer;


