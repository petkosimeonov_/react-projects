import { useGameContext} from "../../context/Context";
import './EndScreen.css';

const prize_table = [
    {level: 15, prize: '100 000'},
    {level: 14, prize: '50 000'},
    {level: 13, prize: '30 000'},
    {level: 12, prize: '20 000'},
    {level: 11, prize: '10 000'},
    {level: 10, prize: '5 000'},
    {level: 9, prize: '3 000'},
    {level: 8, prize: '2 000'},
    {level: 7, prize: '1500'},
    {level: 6, prize: '1000'},
    {level: 5, prize: '500'},
    {level: 4, prize: '400'},
    {level: 3, prize: '300'},
    {level: 2, prize: '200'},
    {level: 1, prize: '100'},
]

const EndScreen = () => {

    const {score} = useGameContext();
    const total_questions = 15;

    return (
        <div className="end-screen">
            {score === total_questions ? (
                <h1>Congratulations, you won!</h1>
            ) : (
                <div className="end-message">
                    <h1>End of the game!</h1>
                    <p>Submitted wrong answer or timed out.</p>
                    <p>Answered questions: {score}</p>
                </div>
            )}
            
            <table className="prize-table">
                <tbody>
                    {prize_table.map(row => (
                        <tr key={row.level} className={score === row.level ? 'highlight-row' : ''}>
                            <td>{row.level}.</td>
                            <td>{row.prize}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
    
}

export default EndScreen;