import audio from '../../public/226000-4f5ac85b-0e0f-4c9e-8027-9c4aca817268.mp3';
import './PlayScreen.css'
import { useRef } from "react";

const Audio = () => {
    const audioRef = useRef<HTMLAudioElement | null>(null);

    const togglePlay = () => {
        if (audioRef.current) {
            if (audioRef.current.paused) {
                audioRef.current.play();
            } else {
                audioRef.current.pause();
            }
        }
    };

    return (
        <div>
            <audio ref={audioRef} src={audio} preload="none" />
            <button className='sound-btn' onClick={togglePlay}>🔊 Sound</button>
        </div>
    );
}

export default Audio;
