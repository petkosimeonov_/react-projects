import { useState, useEffect } from "react";
import './PlayScreen.css'



const Timer = ({ currentQuestionIndex,onTimeout, isPaused}) => {
    const [seconds, setSeconds] = useState(60);

    useEffect(() => {
        
        setSeconds(60);

        const timer = setInterval(() => {
            if(!isPaused) {
            if (seconds > 0) {
                setSeconds(prev => prev - 1)
            }
            else {
                clearInterval(timer);
                onTimeout()
            }
        }
        }, 1000)

        return () => clearInterval(timer)
    }, [currentQuestionIndex, onTimeout, isPaused])


    return (
        <div className="timer">
           <span className="timer-seconds"> {seconds} </span>  seconds
        </div>
    )
}

export default Timer;