import './Jokers.css'

const Audience = ({ correctAnswer, isOpen, onClose }) => {
    if (!isOpen) return null;

    return (
        <div className="modal">
            <div className="modal-content">
                <p>The majority of the audience thinks the correct answer is {correctAnswer}</p>
                <button onClick={onClose}>Close</button>
            </div>
        </div>
    );
};

export default Audience;
