import './Jokers.css'

const CallAFriend = ({ correctAnswer, isOpen, onClose }) => {
    if (!isOpen) return null;

    return (
        <div className="modal">
            <div className="modal-content">
                <p>My friend, I think the correct answer is {correctAnswer}.</p>
                <button onClick={onClose}>Close</button>
            </div>
        </div>
    );
};

export default CallAFriend;
