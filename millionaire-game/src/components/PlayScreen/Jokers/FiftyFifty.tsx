import { useState, useEffect } from "react";
import '../PlayScreen.css';


const FiftyFifty = ({ correctAnswer, incorrectAnswers, selectedAnswer, handleAnswerClick }) => {
    const [remainingAnswers, setRemainingAnswers] = useState([]);

    useEffect(() => {
        const randomIncorrect = incorrectAnswers[Math.floor(Math.random() * incorrectAnswers.length)];
        setRemainingAnswers([correctAnswer, randomIncorrect].sort(() => Math.random() - 0.5));
    }, [correctAnswer, incorrectAnswers]);

    return (
        <div className="answers-grid">
            {remainingAnswers.map(answer => (
                <button
                    key={answer}
                    className={`answer-btn ${
                        selectedAnswer === answer ? 'selected' : ''
                    }`}
                    onClick={() => handleAnswerClick(answer)}
                    disabled={!!selectedAnswer}
                >
                    {answer}
                </button>
            ))}
        </div>
    );
};


export default FiftyFifty;
