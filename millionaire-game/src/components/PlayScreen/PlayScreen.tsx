import { useGameContext } from "../../context/Context";
import { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { END_GAME } from "../../common/constants";
import Audience from "./Jokers/Audience";
import FiftyFifty from "./Jokers/FiftyFifty";
import CallAFriend from "./Jokers/CallFriend";
import Audio from "./Audio";
import Timer from "./Timer";
import './PlayScreen.css';

const PlayScreen = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const questions = location.state?.questions || [];
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState<string | null>(null);
  const [shuffledAnswers, setShuffledAnswers] = useState<string[]>([]);
  const { setScore, } = useGameContext();
  const [isAudienceOpen, setIsAudienceOpen] = useState(false);
  const [isFriendOpen, setIsFriendOpen] = useState(false);
  const [audienceUsed, setAudienceUsed] = useState(false);
  const [isFiftyFiftyUsed, setFiftyFiftyUsed] = useState(false);
  const [isFiftyFiftyActive, setFiftyFiftyActive] = useState(false);
  const [friendUsed, setFriendUsed] = useState(false);


 
 useEffect(() => {
    const currentQuestion = questions[currentQuestionIndex];
    const allAnswers = [
        ... currentQuestion?.incorrect_answers,
        currentQuestion?.correct_answer,
    ].sort(() => Math.random() - 0.5);
    setShuffledAnswers(allAnswers)
 }, [currentQuestionIndex])
 
  useEffect(() => {
    if (selectedAnswer) {
      setTimeout(() => {
        checkAnswer(selectedAnswer);
      }, 2000);
    }
  }, [selectedAnswer]);

  const handleAnswerClick = (answer: string, event?: React.MouseEvent<HTMLButtonElement>) => {
    if (event) {
        const clickedButton = event.currentTarget;
        clickedButton.classList.add("selected");
    }
    setSelectedAnswer(answer);
};


  const checkAnswer = (answer: string) => {
    const correctAnswer = questions[currentQuestionIndex].correct_answer;
    const selectedButton = document.querySelector(`.answer-btn.selected`);

    if (answer === correctAnswer) {
        if (selectedButton) {
            selectedButton.classList.remove("selected");
            selectedButton.classList.add("correct");
        }

        setTimeout(() => {  
            setScore((prev) => prev + 1);
            setSelectedAnswer(null);

            if (currentQuestionIndex < questions.length - 1) {
                setCurrentQuestionIndex((prev) => prev + 1);
            } else {
                navigate(`${END_GAME}`);
            }
            setFiftyFiftyActive(false);
        }, 3000); 
    } else {
        if (selectedButton) {
            selectedButton.classList.remove("selected");
            selectedButton.classList.add("incorrect");
        }

        const correctAnswerButton = Array.from(document.querySelectorAll('.answer-btn')).find(btn => btn.textContent === correctAnswer);
        if (correctAnswerButton) {
            correctAnswerButton.classList.add("correct");
        }

        setTimeout(() => {
            setSelectedAnswer(null);
            navigate(`${END_GAME}`);
        }, 2000);
    }
};

    const handleAudience = () => {
        setIsAudienceOpen(true);
        setAudienceUsed(true)
    }

    const handleFiftyFifty = () => {
        setFiftyFiftyUsed(true);
        setFiftyFiftyActive(true);
    }

    const handleCallAFriend = () => {
        setIsFriendOpen(true);
        setFriendUsed(true);
    }

    const decodeHTML = (text: string) => {
        const textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    }


    return (
        <div className="play-screen">
            <div className="top-elements">
                <div className="jokers">
                    <button onClick={handleAudience} disabled={audienceUsed}>Ask the Audience</button>
                    <button onClick={handleFiftyFifty} disabled={isFiftyFiftyUsed}>50/50</button>
                    <button onClick={handleCallAFriend} disabled={friendUsed}>Call a Friend</button>
                </div>
                <div className="timer-audio">
                    <Audio/>
                    <Timer
                        currentQuestionIndex={currentQuestionIndex}
                        onTimeout={() => {
                            setSelectedAnswer(null);
                            navigate(`${END_GAME}`)
                        }}
                        isPaused={!!selectedAnswer}
                    />
                </div>
            </div>
    
            <Audience correctAnswer={questions[currentQuestionIndex]?.correct_answer} isOpen={isAudienceOpen} onClose={() => setIsAudienceOpen(false)} />
            <CallAFriend correctAnswer={questions[currentQuestionIndex]?.correct_answer} isOpen={isFriendOpen} onClose={() => setIsFriendOpen(false)} />
            
            <div className="question-box">{decodeHTML(questions[currentQuestionIndex]?.question)}</div>
            <div className="answers-grid">
                {isFiftyFiftyActive
                    ? <FiftyFifty 
                        correctAnswer={questions[currentQuestionIndex]?.correct_answer} 
                        incorrectAnswers={questions[currentQuestionIndex]?.incorrect_answers}
                        selectedAnswer={selectedAnswer}
                        handleAnswerClick={handleAnswerClick} />
                    : shuffledAnswers.map((answer, index) => (
                        <button
                            key={index}
                            className={`answer-btn ${selectedAnswer === answer ? 'selected' : ''}`}
                            onClick={(e) => handleAnswerClick(answer,e)}
                            disabled={!!selectedAnswer}
                        >
                            {answer}
                        </button>
                    ))
                }
            </div>
        </div>
    );
    


};

export default PlayScreen;
