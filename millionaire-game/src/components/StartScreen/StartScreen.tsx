import { useNavigate } from "react-router-dom";
import { useGameContext } from "../../context/Context";
import { PLAY_PAGE, BASE_URL_API, EASY_DIFF, MEDIUM_DIFF, HARD_DIFF, SPORTS, HISTORY, GEOGRAPHY, CATEGORY_MAP, Category } from "../../common/constants";
import './StartScreen.css'

const StartScreen = () => {
    const navigate = useNavigate();
    const {difficulty, setDifficulty, category, setCategory} = useGameContext()



    const generateURL = (difficulty: string | null, category: string | null) => {
        let url = BASE_URL_API;

        if (category) {
            url += `&category=${category}`;
        }
    
        if (difficulty) {
            url += `&difficulty=${difficulty}`;
        }
    
        url += `&type=multiple`;
    
        return url;
    }


    const startGame = () => {
        
        const url = generateURL(difficulty, category);
    
        fetch(url)
        .then((response) => response.json())
        .then((data) => {
            navigate(`${PLAY_PAGE}`, {state: {questions: data.results}})
        })
        .catch((err) => {
            console.log(err.message)
        })
    }

    return (
        <div className="start-screen">
            <p className="text-white"> Difficulty: </p>
            <div className="dropdown-container">
                <button className="dropdown-btn">{difficulty || "Select"}</button>
                <div className="dropdown-content">
                    <button onClick={() => setDifficulty(EASY_DIFF)}>Easy</button>
                    <button onClick={() => setDifficulty(MEDIUM_DIFF)}>Medium</button>
                    <button onClick={() => setDifficulty(HARD_DIFF)}>Hard</button>
                </div>
            </div>

            <p className="text-white"> Category: </p>
            <div className="dropdown-container">
                <button className="dropdown-btn">{CATEGORY_MAP[category as Category] || "Select"}</button>
                <div className="dropdown-content">
                    <button onClick={() => setCategory(SPORTS)}>Sports</button>
                    <button onClick={() => setCategory(GEOGRAPHY)}>Geography</button>
                    <button onClick={() => setCategory(HISTORY)}>History</button>
                </div>
            </div>

            <button className="start-game-btn" onClick={startGame}>Start Game</button>
        </div>
    );
}


export default StartScreen;