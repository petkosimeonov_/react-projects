import { createContext, useContext } from "react";


interface GameContextType {
    difficulty: string | null;
    setDifficulty: (value: string | null) => void;
    category: string | null;
    setCategory: (value: string | null) => void;
    score: number;
    setScore: (value: number) => void;
}



export const GameContext = createContext<GameContextType | undefined>(undefined);

export const useGameContext = () => {
    const context = useContext(GameContext)
    if(!context) {
        throw new Error ('No context provided, must be used with GameProvider')
    }
    return context;
}

