import {Route, Routes} from 'react-router-dom';
import StartScreen from './components/StartScreen/StartScreen';
import PlayScreen from './components/PlayScreen/PlayScreen';
import EndScreen from './components/EndScreen/EndScreen';
import { GameContext } from './context/Context';
import {useState} from 'react';



const App = () => {

  const [difficulty, setDifficulty] = useState<string | null>(null);
    const [category, setCategory] = useState<string | null>(null);
    const [score, setScore] = useState<number>(0);
  
  return (
    <GameContext.Provider value={{ difficulty, setDifficulty, category, setCategory, score, setScore}}>
    <Routes>
      <Route path='/' element={<StartScreen/>}/>
      <Route path='/play' element={<PlayScreen/>}/>
      <Route path='/end' element={<EndScreen/>}/>
    </Routes>
    </GameContext.Provider>

  )
}

export default App;