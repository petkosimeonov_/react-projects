export const BASE_URL_API= 'https://opentdb.com/api.php?amount=15';

export const START_PAGE = '/';

export const PLAY_PAGE = '/play';

export const END_GAME = '/end';

export type Difficulty = 'easy' | 'medium' | 'hard';

export const EASY_DIFF: Difficulty = 'easy';

export const MEDIUM_DIFF: Difficulty = 'medium';

export const HARD_DIFF: Difficulty = 'hard';

export type Category = '21' | '22' | '23';

export const SPORTS: Category = '21';

export const GEOGRAPHY: Category = '22';

export const HISTORY: Category = '23';

export const CATEGORY_MAP: Record<Category, string> = {
    '21': 'Sports',
    '22': 'Geography',
    '23': 'History'
};



