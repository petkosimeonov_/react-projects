import { useState } from "react";
import ExpenseList from "./expense-tracker/components/ExpenseList";
import ExpenseFilter from "./expense-tracker/components/ExpenseFilter";
import ExpenseForm from "./expense-tracker/components/ExpenseForm";

export const categories = ["Food", "Clothing", "Technology", "Cars"];

function App() {
  let id = 0;
  const nextId = () => id++;

  const expensesData = [
    { id: nextId(), description: "Orange", amount: 10, category: "Food" },
    { id: nextId(), description: "Shirt", amount: 5, category: "Clothing" },
    { id: nextId(), description: "PS5", amount: 12, category: "Technology" },
    { id: nextId(), description: "BMW", amount: 2, category: "Cars" },
  ];

  const [expenses, setExpenses] = useState(expensesData);
  const [selectedCategory, setSelectedCategory] = useState("");

  const visibleExpenses = selectedCategory
    ? expenses.filter((e) => e.category === selectedCategory)
    : expenses;

  return (
    <div>
      <div className="mb-5">
        <ExpenseForm/>
      </div>
      <div className="mb-3">
        <ExpenseFilter
          onSelectCategory={(category) => setSelectedCategory(category)}
        />
      </div>
      <ExpenseList
        expenses={visibleExpenses}
        onDelete={(id) => setExpenses(expenses.filter((e) => e.id !== id))}
      />
    </div>
  );
}

export default App;
